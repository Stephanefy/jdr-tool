import React from 'react'
import { useEffect } from 'react'
import {
    useSlateStatic
} from 'slate-react'

const InsertImageButton = ({ setOpen, url, setIsImage, setIsAudio }) => {
    const editor = useSlateStatic()

    useEffect(() => {
        console.log(url)
    }, [url])

    return (
        <button
            className='px-2 shadow-lg'
            onMouseDown={(event) => {
                event.preventDefault()
                console.log(event)
                setOpen(true)
                setIsImage(true)
                setIsAudio(false)
            }}
        >
            <i className="fas fa-image"></i>
        </button>
    )
}


export default InsertImageButton