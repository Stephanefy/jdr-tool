import React from 'react'
import { useEffect } from 'react'
import {
    useSlateStatic
} from 'slate-react'
import { insertVideo } from '../RichTextEditor'


const InsertVideoButton = ({ setOpen, url, setIsAudio, setIsImage, setIsVideo }) => {
    const editor = useSlateStatic()

    useEffect(() => {
        console.log(url)
    }, [url])

    return (
        <button
            className='mx-2 px-2 shadow-lg'
            onMouseDown={(event) => {
                event.preventDefault()
                console.log(event)
                setOpen(true)
                setIsVideo(true)
                setIsImage(false)
                setIsAudio(false)

            }}
        >
            <i className="fas fa-video"></i>
        </button>
    )
}

export default InsertVideoButton