import React from 'react'
import { useEffect } from 'react'
import {
    useSlateStatic
} from 'slate-react'

const InsertAudioButton = ({ setOpen, url, setIsAudio, setIsImage }) => {
    const editor = useSlateStatic()

    useEffect(() => {
        console.log(url)
    }, [url])

    return (
        <button
            className='mx-2 px-2 shadow-lg'
            onMouseDown={(event) => {
                event.preventDefault()
                console.log(event)
                setOpen(true)
                setIsAudio(true)
                setIsImage(false)
            }}
        >
            <i className="fas fa-music"></i>
        </button>
    )
}

export default InsertAudioButton