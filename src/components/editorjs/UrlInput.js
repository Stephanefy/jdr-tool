import React from 'react'

const UrlInput = ({ url, onChange }) => {
    const [value, setValue] = React.useState(url)
    return (
      <input
        value={value}
        className="bg-slate-400"
        onClick={e => e.stopPropagation()}
        style={{
          marginTop: '5px',
          boxSizing: 'border-box',
        }}
        onChange={e => {
          const newUrl = e.target.value
          setValue(newUrl)
          onChange(newUrl)
        }}
      />
    )
  }


export default UrlInput