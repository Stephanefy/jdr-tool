import React from 'react'
import {
    Transforms,
    Element as SlateElement,
    
  } from 'slate'
import {
    Slate,
    Editable,
    withReact,
    useSlateStatic,
    ReactEditor,
    useSelected,
    useFocused
  } from 'slate-react'

  import YouTube  from 'react-youtube';



const VideoElement = ({ attributes, children, element }) => {
    const editor = useSlateStatic()

    const path = ReactEditor.findPath(editor, element)

    const selected = useSelected()
    const focused = useFocused()

    const { url } = element

    console.log(url.split)

    return (
      <div {...attributes} className="-z-10 mb-10">
        <div contentEditable={false} className="flex justify-start">
          <YouTube 
            videoId={url.split('=')[1]} 
            style={{
              height: "100%",
              width: "100%",
              overflow:'hidden'
            }}                    // defaults -> {}
  
            />
        </div>
        <div className='flex justfiy-end'>

              <button
                active
                onClick={() => Transforms.removeNodes(editor, { at: path })}
                className={`bg-white`}
              >
                <i className='fas fa-trash text-red-500'></i>
              </button>

        </div>
          {children}

      </div>
    )
  }

export default VideoElement