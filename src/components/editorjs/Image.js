import {
    Slate,
    Editable,
    useSlateStatic,
    useSelected,
    useFocused,
    withReact,
    ReactEditor,
  } from 'slate-react'
  import { Transforms  } from 'slate'

const Image = ({ attributes, children, element, isReadOnly }) => {
    const editor = useSlateStatic()
    const path = ReactEditor.findPath(editor, element)
  
    const selected = useSelected()
    const focused = useFocused()
    return (
      <div {...attributes}>
        <div
          contentEditable={false}
          className='relative'
        >
          <img
            src={element.url}
            className='max-w-xl shadow-lg'
            alt="rich-img"
          />
          {
            !isReadOnly && (
              <button
                active
                onClick={() => Transforms.removeNodes(editor, { at: path })}
                className={`${selected && focused ? 'inline' : 'none'} absolute top-0 right-0 m-2 bg-white`}
              >
                <i className='fas fa-trash'></i>
              </button>
            )
          }
        </div>
        {children}
      </div>
    )
  }


  export default Image