import React from 'react'
import {
    Transforms,
    Element as SlateElement,
    
  } from 'slate'
import {
    Slate,
    Editable,
    withReact,
    useSlateStatic,
    ReactEditor,
    useSelected,
    useFocused
  } from 'slate-react'






const AudioElement = ({ attributes, children, element }) => {
    const editor = useSlateStatic()

    const path = ReactEditor.findPath(editor, element)

    const selected = useSelected()
    const focused = useFocused()

    const { url } = element
    return (
      <div {...attributes}>
        <div contentEditable={false}>
          <div
          >
            <audio
            controls
            >
                <source src={element.url}/>
            </audio>
          </div>
              <button
                active
                onClick={() => Transforms.removeNodes(editor, { at: path })}
                className={`${selected && focused ? 'inline' : 'none'} absolute top-0 right-0 m-2 bg-white`}
              >
                <i className='fas fa-trash'></i>
              </button>
        </div>


        {children}
      </div>
    )
  }

export default AudioElement