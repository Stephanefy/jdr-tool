import React, {
    useState,
    useCallback,
    useEffect,
    useMemo,
    useContext,
} from 'react'
import isHotkey from 'is-hotkey'
import {
    Editable,
    withReact,
    useSlate,
    Slate,
    useSlateStatic,
} from 'slate-react'
import imageExtensions from 'image-extensions'
import isUrl from 'is-url'
import Image from './Image'
import BaseModal from '../modals/BaseModal'
import InsertImageButton from './customButtons/ImageButton'
import InsertAudioButton from './customButtons/AudioButton'
import BlockButton from './customButtons/BlockButton'
import InsertVideoButton from './customButtons/VideoButton';
import { nanoid } from 'nanoid'


import {
    Editor,
    Transforms,
    createEditor,
    Descendant,
    Element as SlateElement,
} from 'slate'
import { withHistory } from 'slate-history'
import AudioElement from './AuidoElement'
import VideoElement from './VideoElement'

const HOTKEYS = {
    'mod+b': 'bold',
    'mod+i': 'italic',
    'mod+u': 'underline',
    'mod+`': 'code',
}

const RichTextExample = ({
    setOpen,
    url,
    setUrl,
    open,
    sceneInfo,
    isReadOnly,
}) => {
    const [isImage, setIsImage] = useState(false)
    const [isAudio, setIsAudio] = useState(false)
    const [isVideo, setIsVideo] = useState(false)

    const renderElement = useCallback(
        (props) => <Element {...props} isReadOnly={isReadOnly} />,
        []
    )
    const renderLeaf = useCallback((props) => <Leaf {...props} />, [])
    const editor = useMemo(
        () => withEmbeds(withImages(withHistory(withReact(createEditor())))),
        []
    )

    const initialValue = useMemo(() => {
        return (
            JSON.parse(localStorage.getItem('content')) || [
                {
                    type: 'paragraph',
                    children: [{ text: '' }],
                },
            ]
        )
    }, [])

    return (
        <Slate
            editor={editor}
            value={sceneInfo ? sceneInfo : initialValue}
            onChange={(value) => {
                const isAstChange = editor.operations.some(
                    (op) => 'set_selection' !== 'op.type'
                )

                if (isAstChange) {
                    const content = JSON.stringify(value)
                    localStorage.setItem('content', content)
                }
            }}
        >
            {!isReadOnly && (
                <div className="bg-slate-50 p-4">
                    <MarkButton format="bold" icon="fas fa-bold" />
                    <MarkButton format="italic" icon="fas fa-italic" />
                    <MarkButton format="underline" icon="fas fa-underline" />
                    <MarkButton format="code" icon="fas fa-code" />
                    <InsertImageButton
                        setOpen={setOpen}
                        url={url}
                        setIsImage={setIsImage}
                        setIsAudio={setIsAudio}
                    />
                    <InsertAudioButton
                        setOpen={setOpen}
                        url={url}
                        setIsAudio={setIsAudio}
                        setIsImage={setIsImage}
                    />
                    <InsertVideoButton
                        setOpen={setOpen}
                        url={url}
                        setIsAudio={setIsAudio}
                        setIsImage={setIsImage}
                        setIsVideo={setIsVideo}
                    />
                    <BlockButton
                        format="heading-one"
                        icon="fas fa-heading"
                        isHeading
                    />
                    <BlockButton
                        format="heading-two"
                        icon="fas fa-heading"
                        isHeading2
                    />
                    <BlockButton format="block-quote" icon="fas fa-quote" />
                    <BlockButton format="numbered-list" icon="fas fa-list-ol" />
                    <BlockButton format="bulleted-list" icon="fas fa-list" />
                    <BlockButton format="left" icon="fas fa-align-left" />
                    <BlockButton format="center" icon="fas fa-align-center" />
                    <BlockButton format="right" icon="fas fa-align-right" />
                    <BlockButton format="justify" icon="fas fa-align-justify" />
                </div>
            )}
            <hr />
            <Editable
                className="p-6 w-12/12 mt-2 shadow-lg mx-auto"
                renderElement={renderElement}
                renderLeaf={renderLeaf}
                placeholder="Entrez votre description du chapitre"
                spellCheck
                autoFocus
                readOnly={isReadOnly ? true : false}
                onKeyDown={(event) => {
                    for (const hotkey in HOTKEYS) {
                        if (isHotkey(hotkey, event)) {
                            event.preventDefault()
                            const mark = HOTKEYS[hotkey]
                            toggleMark(editor, mark)
                        }
                    }
                }}
            />
            {open && (
                <BaseModal
                    setOpen={setOpen}
                    setUrl={setUrl}
                    url={url}
                    isImage={isImage}
                    isAudio={isAudio}
                    isVideo={isVideo}
                />
            )}
        </Slate>
    )
}

const withEmbeds = (editor) => {
    const { isVoid } = editor

    editor.isVoid = (element) =>
        element.type === 'audio' ? true : isVoid(element)
    editor.isVoid = (element) =>
        element.type === 'video' ? true : isVoid(element)

    editor.insertData = (data) => {
        insertAudio(editor, data.getData('text/plain'))
        insertVideo(editor, data.getData('text/plain'))
    }

    return editor
}

export const insertAudio = (editor, url) => {
    console.log('from instertAudio', editor)

    const text = { text: '' }
    const audio = { nodeId: nanoid(), type: 'audio', url, children: [text] }
    Transforms.insertNodes(editor, audio)
}

const withImages = (editor) => {
    const { insertData, isVoid } = editor

    editor.isVoid = (element) => {
        return element.type === 'image' ? true : isVoid(element)
    }

    editor.insertData = (data) => {
        const text = data.getData('text/plain')
        const { files } = data

        if (files && files.length > 0) {
            for (const file of files) {
                const reader = new FileReader()
                const [mime] = file.type.split('/')

                if (mime === 'image') {
                    reader.addEventListener('load', () => {
                        const url = reader.result
                        insertImage(editor, url)
                    })

                    reader.readAsDataURL(file)
                }
            }
        } else if (isImageUrl(text)) {
            insertImage(editor, text)
        } else {
            insertData(data)
        }
    }

    return editor
}

export const insertImage = (editor, url) => {
    const text = { text: '' }
    const image = { nodeId: nanoid(), type: 'image', url, children: [text] }
    Transforms.insertNodes(editor, image)
}

export const insertVideo = (editor, url) => { 


    console.log('from insertVideo', editor)

    const text = {text: ''}
    const video = {nodeId: nanoid(), type: 'video', url, children:[text]}
    Transforms.insertNodes(editor, video)
}

const isImageUrl = (url) => {
    if (!url) return false
    if (!isUrl(url)) return false
    const ext = new URL(url).pathname.split('.').pop()
    return imageExtensions.includes(ext)
}

const toggleMark = (editor, format) => {
    const isActive = isMarkActive(editor, format)

    if (isActive) {
        Editor.removeMark(editor, format)
    } else {
        Editor.addMark(editor, format, true)
    }
}

const isMarkActive = (editor, format) => {
    const marks = Editor.marks(editor)
    return marks ? marks[format] === true : false
}

const Element = (props) => {
    const { attributes, children, element } = props

    const style = { textAlign: element.align }

    switch (element.type) {
        case 'image':
            return <Image {...props} />
        case 'audio':
            return <AudioElement {...props} />
        case 'video':
            return (
                    <VideoElement {...props} />
            
            )
        case 'block-quote':
            return (
                <blockquote style={style} {...attributes}>
                    {children}
                </blockquote>
            )
        case 'bulleted-list':
            return (
                <ul style={style} className="list-disc mx-2" {...attributes}>
                    {children}
                </ul>
            )
        case 'heading-one':
            return (
                <h1 style={style} className="text-5xl" {...attributes}>
                    {children}
                </h1>
            )
        case 'heading-two':
            return (
                <h2 style={style} className="text-3xl" {...attributes}>
                    {children}
                </h2>
            )
        case 'list-item':
            return (
                <li style={style} {...attributes}>
                    {children}
                </li>
            )
        case 'numbered-list':
            return (
                <ol
                    style={style}
                    className="list-decimal ml-24"
                    {...attributes}
                >
                    {children}
                </ol>
            )
        default:
            return (
                <p style={style} className="my-2" {...attributes}>
                    {children}
                </p>
            )
    }
}

const Leaf = ({ attributes, children, leaf }) => {
    if (leaf.bold) {
        children = <strong>{children}</strong>
    }

    if (leaf.code) {
        children = <code>{children}</code>
    }

    if (leaf.italic) {
        children = <em>{children}</em>
    }

    if (leaf.underline) {
        children = <u>{children}</u>
    }

    return <span {...attributes}>{children}</span>
}

const MarkButton = ({ format, icon }) => {
    const editor = useSlate()

    useEffect(() => {
        console.log(icon)
    }, [])

    return (
        <button
            className="px-2 shadow-lg"
            active={isMarkActive(editor, format)}
            onMouseDown={(event) => {
                event.preventDefault()
                toggleMark(editor, format)
            }}
        >
            <i className={icon}></i>
        </button>
    )
}

export default RichTextExample
