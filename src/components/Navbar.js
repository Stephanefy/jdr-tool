import React from 'react'
import { Link } from 'react-router-dom'
import {useLogout} from '../hooks/useLogout'
import { useAuthContext } from '../hooks/useAuthContext'
import Logo from '../images/logo.svg'
import styles from './Navbar.module.css'



const Navbar = () => {
    
    
  const { logout } = useLogout()
  const { user } = useAuthContext()

  return (
    <nav className={styles.navbar}>
        <ul>
            <li className={styles.title}>
              <Link to="/">
                <img src={Logo} alt="logo" width={80} />
              </Link>
            </li>
            {
              !user && (
              <>
                <li><Link to="/connexion" className='btn'>Connexion</Link></li>
                <li><Link to="/s'enregistrer" className='btn'>S'enregistrer</Link></li>
              </>
              )
            }
            {
              user && (
              <>
                <li>Hello, {user.displayName}</li>
                <li>
                  <button onClick={logout} className="btn">Déconnexion</button>
                </li>

              </>

              )
            }
        </ul>
    </nav>
    
  )
}

export default Navbar