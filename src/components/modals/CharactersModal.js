import React, { useState } from 'react'
import TraitInput from '../MasterDashboard/traitsInput/TraitInput';
import { useTraitInputs } from '../../hooks/useTraitInputs';

const CharactersModal = ({open, setOpen, character, characterInfo, setCharacterInfo, initialCharacterInfo}) => {

    

    const [traitInputNum, setTraitInputNum] = useState(0)

    const { traitInputs, setTraitInputs } = useTraitInputs(traitInputNum, setTraitInputNum)

    const handleChange = (e) => {
        console.log(e)
    }

    return (
        <div
            className={`h-fit bg-slate-800 bg-opacity-50 flex justify-center items-center absolute top-0 right-0 bottom-0 left-0 z-50`}
        >
            <div className="relative h-full bg-white w-11/12 px-8 md:px-16 py-14 rounded-md text-center flex flex-col my-36">
                <h1 className="text-xl mb-4 font-bold text-slate-500">
                    Modifier le personnage
                </h1>
                    <input type="text" value={character?.name} className="border-2 my-2" onChange={handleChange} />
                    <textarea name="story" id="" cols="30" rows="10">{character?.story}</textarea>
                    <h2>Modifier les attributs existant</h2>
                    {
                        character.attributes.map(e => (
                            <TraitInput key={e.id} trait={e} isUpdate setCharacterInfo={setCharacterInfo} initialCharacterInfo={initialCharacterInfo} />
                        ))
                    }
                    <h2>Ajouter de nouveaux attributs</h2>
                    <div>
                        <input
                            className='hidden'
                            type="number"
                            min={1}
                            value={traitInputNum}
                        />
                        <button
                            onClick={(e) => {
                                e.preventDefault()
                                setTraitInputNum(traitInputNum + 1)
                            }}
                            className="absolute bottom-20 right-6 p-2 bg-blue-500 text-white rounded-full m-10"
                        >
                            + attribut
                        </button>
                    </div>
                    <div>
                        {traitInputs.length > 0 && (
                            <p>Définissez les attributs</p>
                        )}
                        {traitInputs.map((e) => e)}
                    </div>
                    <div className="flex justify-between mt-24">
                        <button
                            className="bg-red-500 px-4 py-2 rounded-md text-md text-white"
                            onClick={() => setOpen(false)}
                        >
                            Annuler
                        </button>
                        <button
                            onClick={() => setOpen(false)}
                            type="submit"
                            className="bg-indigo-500 px-7 py-2 ml-2 rounded-md text-md text-white font-semibold"
                        >
                            Ok
                        </button>
                    </div>
            </div>
        </div>
    )
}

export default CharactersModal
