import React from 'react'
import { useSlateStatic } from 'slate-react';
import { insertImage } from '../editorjs/RichTextEditor';
import { insertAudio } from '../editorjs/RichTextEditor';
import { insertVideo } from '../editorjs/RichTextEditor';

const BaseModal = ({ setOpen, setUrl, url, isImage, isAudio, isVideo }) => {


    const editor = useSlateStatic();

    const handleSubmit = (e) => {
        e.preventDefault();
        
        if (isImage) {
            insertImage(editor, url);
        } else if (isAudio) {
            insertAudio(editor, url)

        } else if (isVideo) {
            insertVideo(editor, url)
        }
        setUrl("")
        setOpen(false)
    }

    const handleChange = (e) => {
        setUrl(e.target.value)
    }


    return (
        <div
            className={`flex justify-center items-center absolute top-0 right-0 bottom-0 left-0`}
        >
            <div className="bg-white shadow-lg px-16 py-14 rounded-md text-center">
                <h1 className="text-xl mb-4 font-bold text-slate-500">
                    {isImage && "URL de l'image"}
                    {isAudio && "URL de l'audio"}
                    {isVideo && "Url de la vidéo"}
                </h1>
                <form className="flex flex-col" onSubmit={handleSubmit}>
                    <input type="text" value={url} className="border-2" onChange={handleChange} />
                    <div className="flex justify-between mt-6">
                        <button
                            className="bg-red-500 px-4 py-2 rounded-md text-md text-white"
                            onClick={() => setOpen(false)}
                        >
                            Annuler
                        </button>
                        <button
                            type="submit"
                            className="bg-indigo-500 px-7 py-2 ml-2 rounded-md text-md text-white font-semibold"
                        >
                            Ok
                        </button>
                    </div>
                </form>
            </div>
        </div>
    )
}

export default BaseModal
