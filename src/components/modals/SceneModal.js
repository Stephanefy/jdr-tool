import React, { useState } from 'react'
import RichTextExample from '../editorjs/RichTextEditor'

const SceneModal = ({open, setOpen, character, characterInfo, setCharacterInfo, initialCharacterInfo, sceneInfo}) => {

    

    const handleChange = (e) => {
        console.log(e)
    }

    return (
        <div
            className={`bg-slate-800 bg-opacity-50 flex justify-center items-center absolute top-0 right-0 left-0 z-50`}
        >
            <div className="relative bg-white w-12/12 px-8 py-14 rounded-md text-center flex flex-col my-36">
                <h1 className="text-xl mb-4 font-bold text-slate-500">
                    Modifier la scène
                </h1>
                <RichTextExample sceneInfo={sceneInfo}/>
                    <div className="flex justify-between mt-24">
                        <button
                            className="bg-red-500 px-4 py-2 rounded-md text-md text-white"
                            onClick={() => setOpen(false)}
                        >
                            Annuler
                        </button>
                        <button
                            onClick={() => setOpen(false)}
                            type="submit"
                            className="bg-indigo-500 px-7 py-2 ml-2 rounded-md text-md text-white font-semibold"
                        >
                            Ok
                        </button>
                    </div>
            </div>
        </div>
    )
}

export default SceneModal
