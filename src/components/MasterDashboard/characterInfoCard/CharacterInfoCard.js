import React from 'react'

const CharacterInfoCard = ({character}) => {
  return (
    <div>
        <h4>{character.name}</h4>
        <ul>
            {character.attributes.map(attr => (
                <li key={attr.id}>
                    <span>{attr.name}: {attr.points}</span>
                </li>
            ))}
        </ul>
    </div>
  )
}

export default CharacterInfoCard