import React, { useContext, useState, useEffect, useMemo } from 'react'
import { WorldContext } from '../../context/WorldContext'
import { useTraitInputs } from '../../hooks/useTraitInputs'
import style from './StepTwo.module.css'
import styles from '../../pages/home/Home.module.css'
import cx from 'classnames'
import CharactersModal from '../modals/CharactersModal'

const initialCharacterInfo = {
    name: '',
    story: '',
}

const StepTwo = () => {
    const {
        characterTraits,
        setCharacterTraits,
        globalWorldDetail,
        setGlobalWorldDetail,
        initialCharacterTraits,
    } = useContext(WorldContext)

    const getinitialCharacters = () => {
        const chars =
            JSON.parse(localStorage.getItem('draft'))?.characters ?? []

        if (chars.length === 0) {
            return (
                globalWorldDetail.characters && [
                    ...globalWorldDetail.characters,
                ]
            )
        } else {
            return chars
        }
    }
    // traits input states
    const [traitInputNum, setTraitInputNum] = useState(0)
    const [characterNum, setCharacterNum] = useState(
        globalWorldDetail?.numPlayers
    )
    const { traitInputs, setTraitInputs} = useTraitInputs(traitInputNum, setTraitInputNum )

    // character name and story states
    const [characterInfo, setCharacterInfo] = useState(initialCharacterInfo)
    const [characters, setCharacters] = useState(getinitialCharacters)
    const [open, setOpen] = useState(false)
    const [charEdit, setCharEdit] = useState(null)


    useEffect(() => {
        setCharacterTraits((prev) => ({
            ...prev,
            name: characterInfo?.name,
            story: characterInfo?.story,
        }))
    }, [characterInfo])

    useEffect(() => {
        console.log('global', globalWorldDetail)
    }, [globalWorldDetail])



    const handleAddCharacter = () => {
        if (
            characterInfo.name !== '' &&
            characterInfo.story !== '' &&
            traitInputs.length !== 0
        ) {
            setGlobalWorldDetail((prev) => ({
                ...prev,
                characters: [...prev.characters, characterTraits],
            }))

            setCharacterTraits(initialCharacterTraits)
            setCharacterInfo(initialCharacterInfo)
            setTraitInputs([])
            setTraitInputNum(0)
            setCharacters([...characters, characterTraits])
        }
    }

    const handleRemoveCharacter = (name) => {
        const newCharacters = characters.filter(
            (character) => character?.name !== name
        )

        setGlobalWorldDetail((prev) => ({
            ...prev,
            characters: [...newCharacters],
        }))

        setCharacters([...newCharacters])
    }

    const handleEditCharacter = (char) => {
        setOpen(true)
        setCharEdit({...char})
    }

    return (
        <>
            <section className={style['character-traits-section']}>
                <h2>Nombre de personnages de cet univers:</h2>
                <p className={style['character-number']}>{characterNum}</p>
                {characters.length !== 0 &&
                    characters?.map((char) => (
                        <div className={cx(style['character-card-preview'], ['my-2 flex justify-between'])}>
                            <div>
                                <p className='text-white'>{char?.name}</p>
                                <ul>
                                    {char?.attributes.map((attr) => (
                                        <li className='text-white'>
                                            {attr.name}:{''} {attr.points}
                                        </li>
                                    ))}
                                </ul>
                            </div>
                            <div>
                                <button className='mr-2 text-white' onClick={() => handleEditCharacter(char)}>
                                    <i className="fas fa-edit"></i>
                                </button>
                                <button
                                    className='text-slate-200'
                                    onClick={() => handleRemoveCharacter(char?.name)}
                                >
                                    <i className="fas fa-trash"></i>
                                </button>
                            </div>
                        </div>
                    ))}
                <h2>Ajouter les personnages</h2>

                <form className={style['character-creation']}>
                    <label>Nom du personnage</label>
                    <input
                        className="border-2 border-gray-400 p-2 w-full"
                        type="text"
                        required
                        name="name"
                        value={characterInfo?.name}
                        onChange={(e) =>
                            setCharacterInfo((prev) => ({
                                ...prev,
                                name: e.target.value,
                            }))
                        }
                    />
                    <label>Histoire du personnage</label>
                    <textarea
                        className="border-2 border-gray-400 p-2 w-full"
                        rows={5}
                        cols={100}
                        value={characterInfo?.story}
                        onChange={(e) =>
                            setCharacterInfo((prev) => ({
                                ...prev,
                                story: e.target.value,
                            }))
                        }
                    ></textarea>
                    <h4>Caractéristiques du personnage</h4>
                    <div className={style['traitnum-input-group']}>
                        <input
                            className={style['traitinput-number']}
                            type="number"
                            min={1}
                            value={traitInputNum}
                        />
                        <button
                            onClick={(e) => {
                                e.preventDefault()
                                setTraitInputNum(traitInputNum + 1)
                            }}
                            className="p-2 bg-blue-500 text-white rounded-full m-10"
                        >
                            + attribut
                        </button>
                    </div>
                    <div>
                        {traitInputs.length > 0 && (
                            <p>Définissez les attributs</p>
                        )}
                        {traitInputs.map((e) => e)}
                    </div>
                    {characterInfo.name !== '' &&
                        characterInfo.story !== '' &&
                        characterTraits.attributes.some(
                            (e) => e.validated === true
                        ) && (
                            <div className={style['validate-button-container']}>
                                <button
                                    type="button"
                                    className={
                                        style['css-button-neumorphic-validate']
                                    }
                                    onClick={handleAddCharacter}
                                >
                                    Ajouer ce personnage à l'univers
                                </button>
                            </div>
                        )}
                </form>
                {
                    characters.length === 0 && (
                      
                         <p className={styles['info-msg']}>
                        Veuillez ajouter au moins un personnage à l'univers
            </p>
        )}
          
            </section>
            {
                open && (
                    <CharactersModal 
                        open={open} 
                        setOpen={setOpen} 
                        character={charEdit} 
                        setCharacterInfo={setCharacterInfo} 
                        initialCharacterInfo={initialCharacterInfo}
                        setTraitInputs={setTraitInputs}
                    />
                )
            }
        </>
    )
}

export default StepTwo
