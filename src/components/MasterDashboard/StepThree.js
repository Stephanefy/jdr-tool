import React, {useState, useContext} from 'react';
import { WorldContext } from '../../context/WorldContext';
import style from './StepThree.module.css';
import CharacterInfoCard from './characterInfoCard/CharacterInfoCard';

const StepThree = () => {

  const { globalWorldDetail } = useContext(WorldContext)


  const { world, story, characters } = globalWorldDetail  

  console.log(characters)


  return (
    <section>
        <h1>Récapitulatif</h1>
        <article className={style["summary-card"]}>
            <div className={style["card-body"]}>
                <h2>{world}</h2>
                <h2>Liste des personnages de l'univers</h2>
                <ul>
                    {
                    characters.map(c => (
                        <li className={style['summary-character']}><CharacterInfoCard character={c} /></li>
                        ))
                    }
                </ul>
            </div>
        </article>
    </section>
  )
}

export default StepThree