import React from 'react'
import { truncate } from '../../../utils/functions'

const CharacterCard = ({ character }) => {
    return (
        <div className="bg-white rounded-2xl border shadow-x1 p-6 w-full lg:max-w-md mt-6 mr-6 cursor-pointer">
            <div className="flex flex-col items-center space-y-4">
                <h1 className="font-bold text-2xl text-gray-700 w-4/6 text-center">
                    {character.name}
                </h1>
                <p className='text-sm'>
                    {truncate(character.story, 100)}
                </p>
                <ul>
                    {character.attributes.map((attr) => (
                        <li key={attr.id}>
                            <span>{attr.name}</span>: {attr.points}
                        </li>
                    ))}
                </ul>
            </div>
        </div>
    )
}

export default CharacterCard
