import React,{ useContext, useEffect } from 'react'
import { truncate } from '../../../utils/functions'
import { useHistory } from 'react-router-dom'
import { WorldDetailContext } from '../../../context/WorldDetailContext'
import { useFirestore } from '../../../hooks/useFirestore'

const WorldCard = ({ world }) => {

    const history = useHistory();

    const { deleteDocument } = useFirestore('world')

   
   const { setWorldDetail } = useContext(WorldDetailContext)


    useEffect(() => {
        setWorldDetail({...world})
    },[])


    const handleClick = () => {

        const location = {
            pathname: `/univers/${world.id}`,
            state: { worldInfo: {...world} }
          }

        history.push(location)
    }


    const handleRemoveWorld = () => {
        deleteDocument(world.id)
    }

    return (
        <div className="relative bg-white rounded-2xl border shadow-x1 p-10 max-w-lg mt-6 ml-6">
            <div className="flex flex-col items-center space-y-4">
                <h1 className="font-bold text-2xl text-gray-700 w-4/6 text-center">
                    {world.world}
                </h1>
                <div className='absolute top-0 right-0 mr-6'>
                    <button onClick={handleRemoveWorld}>
                        <i className='fas fa-trash'></i>
                    </button>
                </div>
                <p className="text-sm text-left text-gray-500 w-100">
                    <span>Nombre de personnages: </span>
                    {world.numPlayers}
                </p>
                <p>
                    <span className="text-sm">
                        {truncate(world.story, 100)}{' '}
                    </span>
                </p>
                <div>
                    <button className='css-button-neumorphic mx-3' onClick={handleClick}>Voir détails</button>
                </div>
            </div>
        </div>
    )
}

export default WorldCard
