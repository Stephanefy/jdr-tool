import React from 'react'

const SceneInfoCard = ({ scene }) => {
  return (
    <div className="bg-white rounded-2xl border shadow-x1 p-10 max-w-lg mt-6 mr-6 cursor-pointer">
    <div className="flex flex-col items-center space-y-4">
        <h1 className="font-bold text-2xl text-gray-700 w-4/6 text-center">
            {scene.title}
        </h1>
        <p className='text-sm'>
            {scene.description}
        </p>
    </div>
</div>
  )
}

export default SceneInfoCard