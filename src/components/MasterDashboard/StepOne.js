import React, { useContext, useState, useEffect } from 'react';
import { WorldContext } from '../../context/WorldContext';
import styles from '../../pages/home/Home.module.css'
import WorldPreview from '../../pages/master/WorldPreview';
import cx from 'classnames'


const StepOne = () => {



    const {globalWorldDetail,setGlobalWorldDetail } = useContext(WorldContext)
    const [errors, setErrors] = useState({
        worldError: false,
        numPlayerError: false,
        storyError: false
    })
    const [validated, setValidated] = useState(false)

    const {world, numPlayers, story} = globalWorldDetail

    
    const changeHandler = (e) => {
        e.target.name === "numPlayers" && setGlobalWorldDetail(prev => ({...prev, numPlayers: e.target.value}))
        e.target.name === "world" && setGlobalWorldDetail(prev => ({...prev, world: e.target.value}))
        e.target.name === "story" && setGlobalWorldDetail(prev => ({...prev, story: e.target.value}))
      //   setWorld(e.target.valu  const {world, setWorld, numPlayers, setNumPlayers, story, setStory} = useContext(WorldContext)

  }
    
    const handleValidateWorldDetail = () => {

        
        setGlobalWorldDetail(prev => ({
            ...prev,
            world,
            numPlayers,
            story
        }))
    }
    
    useEffect(() => {

        console.log(validated)

        // validation for each input fields

        if (world) {
            setErrors(prev => ({
                ...prev,
                worldError: false
            }))
        }
        if (numPlayers) {
            setErrors(prev => ({
                ...prev,
                numPlayerError: false
            }))
        }
        if (story) {
            setErrors(prev => ({
                ...prev,
                storyError: false
            }))
        }

        // validation fore disabling or enabling button
        if(world && numPlayers && story) {
            setValidated(true)
        } else {
            if (!world) {
                setErrors(prev => ({
                    ...prev,
                    worldError: true
                }))
            }
            if (!numPlayers) {
                setErrors(prev => ({
                    ...prev,
                    numPlayerError: true
                }))
            }
            if (!story) {
                setErrors(prev => ({
                    ...prev,
                    storyError: true
                }))
            }
            setValidated(false)
        }


    },[world, numPlayers, story])


    console.log(errors)
 

    return (
    <>    
        <h2>Créer un nouvel univers</h2>

        <form className={cx(styles["world-creation"],['w-86 md:w-11/12 lg:w-full'])}>
            <label>Nom de l'univers</label>
            <input 
                type="text" 
                required 
                name="world" 
                value={world} 
                placeholder="univers"
                onChange={changeHandler}
                className={cx({[styles['has-error']] : errors.worldError}, ['border-2 border-gray-200'])}
            />
            <label>Nombre de joueurs</label>
            <input 
                type="number" 
                required 
                name="numPlayers" 
                value={numPlayers} 
                placeholder="entrez un nombre de joueurs"
                onChange={changeHandler}
                className={cx({[styles['has-error']] : errors.numPlayerError }, ['border-2 border-gray-200'])}
            />
            <label>Résumé du scénario</label>
            <textarea 
                type="text" 
                required 
                name="story" 
                value={story} 
                placeholder="résumé"
                onChange={changeHandler}
                cols={100}
                rows={10}
                className={cx({[styles['has-error']] : errors.storyError}, ['border-2 border-gray-200'])}
            />
            <button 
                type='button' 
                disabled={!validated} 
                className={cx(styles['css-button-neumorphic-validate'], ['mt-2'])} 
                onClick={handleValidateWorldDetail}>
                Confirmer cet univers
            </button>
        </form>
        {!validated && (
            <p className={styles['info-msg']}>
                        Veuillez compléter ces champs avant de passer à l'étape
                        suivante
            </p>
        )}
</>
  )
}

export default StepOne