import React, { useState, useContext, useEffect } from 'react'
import { WorldContext } from '../../../context/WorldContext'
import styles from './TraitInput.module.css'
import cx from 'classnames'
import { removeDuplicates } from '../../../utils/functions'

const TraitInput = ({
    id,
    setTraitInputs,
    setTraitInputNum,
    traitInputs,
    trait,
    isUpdate,
    setCharacterInfo,
    initialCharacterInfo,
}) => {
    const {
        characterTraits,
        setCharacterTraits,
        setGlobalWorldDetail,
        globalWorldDetail,
        initialCharacterTraits,
    } = useContext(WorldContext)

    const [traitInfo, setTraitInfo] = useState(
        () =>
            trait || {
                id,
                name: '',
                points: 0,
                validated: false,
            }
    )
    const [error, setError] = useState(null)
    const [traitInfoArr, setTraitInfoArr] = useState([])
    const [validatedTrait, setValidatedTrait] = useState(false)
    const [showDropDown, setShowDropDown] = useState(false)
    const { name, points } = traitInfo
    

    useEffect(() => {
        if (traitInfo.validated) {
            setCharacterTraits((prev) => ({
                ...prev,
                attributes: removeDuplicates(
                    [...prev?.attributes, traitInfo],
                    'id'
                ),
            }))
        }
    }, [validatedTrait])

    const handleDropDown = (e) => {
        e.preventDefault()
        setShowDropDown(!showDropDown)
    }

    const handleAttrValidate = (e) => {
        e.preventDefault()

        if (name === '' || points === 0) {
            setError('Vous devez compléter ces deux champs')
        } else {
            setValidatedTrait(true)
            setTraitInfo((prev) => ({ ...prev, validated: true }))
        }
    }

    const handleUpdateTraits = () => {
        console.log(traitInfo)

        const updatedCharacter = globalWorldDetail.characters.find((char) =>
            char.attributes.find((trait) => trait.id === traitInfo.id)
        )
        const updatedTraitIndex = updatedCharacter.attributes.findIndex(
            (trait) => trait.id === traitInfo.id
        )

        console.log(updatedTraitIndex)

        setGlobalWorldDetail((prev) => ({
            ...prev,
            characters: prev.characters.map((char) => {
                if (char.id === updatedCharacter.id) {
                    char.attributes[updatedTraitIndex] = traitInfo
                }
                return char
            }),
        }))
        setValidatedTrait(true)
    }

    useEffect(() => {
        let timeOut = setTimeout(() => {
            setError(null)
        }, 1500)

        return () => {
            clearTimeout(timeOut)
        }
    }, [error])

    const handleRemoveTraitInput = (e) => {
        e.preventDefault()
        setTraitInputs((prev) =>
            [...prev].filter((obj) => obj.props.id !== id)
        )
        setTraitInputNum(traitInputs.length)
        setCharacterTraits((prev) => ({
            ...prev,
            attributes: prev.attributes?.filter(
                (obj) => obj.id !== id
            ),
        }))
    }

    //   useEffect(() => {
    //     setCharacterTraits(prev => ({
    //         ...prev,
    //         attributes: [...prev.attributes, traitInfo]
    //     }))
    //   }, [traitInfo])

    console.log('fdsfsd', characterTraits)

    return (
        <div
            className={cx(styles.traitInputContainer, {
                [styles.validated]: validatedTrait,
            })}
        >
        <div className='flex justify-end mb-2'>
            <p className={`${validatedTrait ? "text-white" : "text-dark"} text-xl`}>{name}</p>
            <button className="text-white mx-2 " onClick={handleDropDown}>
                {!showDropDown && (<i className="fas fa-angle-up bg-green-600 p-2 rounded-lg"></i>)}
                {showDropDown && (<i className="fas fa-angle-down bg-green-600 p-2 rounded-lg"></i>)}
            </button>
            <button onClick={handleRemoveTraitInput}>
                <i className='fas fa-trash'></i>
            </button>
        </div>
            {showDropDown && (
            <>

                <div className={styles.dropdown}>
                    <div className={styles['input-group']}>
                        <label>
                            <span>Attribut: {name}</span>
                        </label>
                        <input
                            type="text"
                            value={name}
                            onChange={(e) =>
                                setTraitInfo((prev) => ({
                                    ...prev,
                                    name: e.target.value,
                                }))
                            }
                            placeholder="nom de l'attribut"
                            required
                            className={cx({ [styles['has-error']]: error }, [
                                'border-2 border-gray-200',
                            ])}
                        />

                        <label>
                            <span>points:{points} </span>
                        </label>
                        <div className={styles['points-input']}>
                            <input
                                type="number"
                                required
                                name={points}
                                value={points}
                                min={0}
                                // value={characterTraits.bravery}
                                placeholder="nombre de points de l'attribut"
                                // onChange={(e) => setCharacterTraits(prevState => ({...prevState, bravery: e.target.value}))}
                                className={cx(
                                    styles['trait-input'],
                                    { [styles['has-error']]: error },
                                    ['border-2 border-gray-200 w-11/12']
                                )}
                            />
                            <button
                                onClick={(e) => {
                                    e.preventDefault()
                                    setTraitInfo((prev) => ({
                                        ...prev,
                                        points: prev.points + 1,
                                    }))
                                }}
                                className="bg-green-400 px-4 rounded"
                            >
                                +
                            </button>
                            <button
                                onClick={(e) => {
                                    e.preventDefault()
                                    setTraitInfo((prev) => ({
                                        ...prev,
                                        points: prev.points - 1,
                                    }))
                                }}
                                className="bg-green-400 px-4 rounded"
                            >
                                -
                            </button>
                        </div>
                    </div>
                </div>
            <div className={styles['btn-group']}>
                {!isUpdate && (
                    <button
                        onClick={handleRemoveTraitInput}
                        className="text-sm md:text-lg css-button-neumorphic-cancel"
                    >
                        Supprimer
                    </button>
                )}

                <button
                    onClick={isUpdate ? handleUpdateTraits : handleAttrValidate}
                    className="text-sm md:text-lg css-button-neumorphic-validate"
                >
                    {isUpdate ? 'Modifier' : 'Valider'}
                </button>
            </div>
            </>)
            }
            {error && <p className={styles['error']}>{error}</p>}
        </div>
    )
}

export default TraitInput
