import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import { AuthContextProvider} from './context/AuthContext'
import {WorldContextProvider} from './context/WorldContext'
import {WorldDetailContextProvider} from './context/WorldDetailContext'

ReactDOM.render(
  <React.StrictMode>
    <AuthContextProvider>
      <WorldContextProvider>
        <WorldDetailContextProvider>

        <App />
        </WorldDetailContextProvider>

      </WorldContextProvider>
    </AuthContextProvider>
  </React.StrictMode>,
  document.getElementById('root')
);
