export const truncate = (str, n) => {
    return str.length > n ? str.substr(0, n - 1) + '…' : str
} 



export const removeDuplicates = (arr, key) => {
    const unique = arr.reduce(function (acc, curr) {
        console.log('current', curr)

        if (key) {
            if (!acc.some(function (item) {
                return item[key] === curr[key]
            })) {
                acc.push(curr)
            }
        } else {
            
            if (!acc.includes(curr))
                acc.push(curr);
        }
        return acc;
    }, []);
    return unique;
}