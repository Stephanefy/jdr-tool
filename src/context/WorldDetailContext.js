import { createContext, useEffect, useState, useMemo} from "react";
import { projectFirestore } from "../firebase/config";

export const WorldDetailContext = createContext({});





export const WorldDetailContextProvider = ({children}) => {
    
    const [worldDetail, setWorldDetail] = useState(null)


    useEffect(() => {
        console.log(worldDetail)
    },[worldDetail])


    const value = useMemo(
        () => ({
            worldDetail,
            setWorldDetail
        }), 
        [worldDetail]
     );

    return (
        <WorldDetailContext.Provider value={value}>
            {children}
        </WorldDetailContext.Provider>
    )

}