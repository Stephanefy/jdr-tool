import { createContext, useEffect, useState, useMemo} from "react";
import { projectFirestore } from "../firebase/config";

export const WorldContext = createContext({
  world: '',
  setWorld: () => {},
  numPlayers: 0,
  setNumPlayers : () => {},
  story: '',
  setStory: () => {},
  step: 1,
  setStep: () => {},
  characterTraits: {},
  setCharacterTraits: () => {}
});



const initialCharacterTraits = {
    name: "",
    story: "",
    attributes: []
}


export const WorldContextProvider = ({children}) => {
    
    
    const initialGlobalWorldDetail = useMemo(() => {
        return JSON.parse(localStorage.getItem('draft'))
    },[])

    const [step, setStep] = useState(1)
    const [characterTraits, setCharacterTraits] = useState(initialCharacterTraits)


    const [globalWorldDetail, setGlobalWorldDetail] = useState(initialGlobalWorldDetail ?? {
        world: "",
        numPlayers: 0,
        story:"",
        characters: []
    })


    useEffect(() => {
        localStorage.setItem('draft', JSON.stringify(globalWorldDetail))
    },[globalWorldDetail])


    useEffect(() => {
        console.log(globalWorldDetail)
    },[globalWorldDetail])

    const value = useMemo(
        () => ({ 
            step,
            setStep,
            characterTraits,
            setCharacterTraits,
            setGlobalWorldDetail,
            globalWorldDetail,
            initialCharacterTraits,
            
        }), 
        [globalWorldDetail, step, characterTraits]
     );

    return (
        <WorldContext.Provider value={value}>
            {children}
        </WorldContext.Provider>
    )

}