import { useEffect, useState } from 'react';
import { projectAuth } from '../firebase/config';
import { useAuthContext } from './useAuthContext';


export const useLogin = () => {

    const [error, setError] = useState(null)
    const [isPending, setIsPending] = useState(false);
    const [isCancelled, setIsCancelled] = useState(false)
    const { dispatch } = useAuthContext()

    const login = async (email, password) => {
        setError(null)
        setIsPending(true)


        // sign user out
        try{

            const response = await projectAuth.signInWithEmailAndPassword(email, password)


            // dispatch logout action
            dispatch({
                type: 'LOGIN',
                payload: response.user
            })

            // update state
            if (!isCancelled) {
                setIsPending(false)
                setError(null)
            }

        } catch(err) {
            if (!isCancelled) {
                console.error(err)
                setError(err.message)
                setIsPending(false)
            }
        }
    }


    useEffect(() => {
        
        return () => setIsCancelled(true)
    },[])

    return {
        login,
        error,
        isPending
    }
    
}