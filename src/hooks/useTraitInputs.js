import { useState, useEffect } from 'react';
import TraitInput from '../components/MasterDashboard/traitsInput/TraitInput';


export const useTraitInputs = (traitInputNum, setTraitInputNum) => {

    const [traitInputs, setTraitInputs] = useState([])

    useEffect(() => {
        if (traitInputNum === traitInputs.length) return

        traitInputNum >= 1 &&
            setTraitInputs((prev) => [
                ...prev,
                <TraitInput
                    id={Math.floor(Math.random() * 100)}
                    setTraitInputs={setTraitInputs}
                    setTraitInputNum={setTraitInputNum}
                    traitInputs={traitInputs}
                />,
            ])
    }, [traitInputNum])


    return {
        traitInputs,
        setTraitInputs
    }

}