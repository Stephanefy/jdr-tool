import { useReducer, useEffect, useState} from 'react';
import { projectFirestore, timestamp } from '../firebase/config';

let initialState = {
    document: null,
    isPending: false,
    error: null,
    success: null
}

const fireStoreReducer = (state, action) => {
    switch (action.type) {
        
        case "IS_PENDING":
            return initialState
        case "ADDED_DOCUMENT":
            return {isPending: false, document: action.payload, success: true, error: null}
        case "GET_DOCUMENT":
            return {isPending: false, document: action.payload, success: true, error: null}
        case "DELETED_DOCUMENT":
            return {isPending: false, document: null, success: true, error: null}
        
        case "ERROR":
            return {isPending: false, document: null, success: false, error: action.payload}
        
        default:
            return state
    }
}




export const useFirestore = (collection) => {
    const [response, dispatch] = useReducer(fireStoreReducer, initialState)
    const [isCancelled, setIsCancelled] = useState(false)


    const ref = projectFirestore.collection(collection)
    
    // only dispatch if not cancelled
    const dispatchIfNotCancelled = (action) => {
        if(!isCancelled) {
            dispatch(action)
        }
    }

    // add collection
    const addCollection = async (doc) => {

        //firestore add collection
        projectFirestore.collection(collection).doc().set({
            world: doc.world,
            numPlayers: doc.numPlayers,
            characters: doc.characters,
            uid: doc.uid,
            story: doc.story,
        })
        .then(() => {
            console.log("Document successfully written!");
        })
        .catch((error) => {
            console.error("Error writing document: ", error);
        });


    }

    // add collection
    const addGenericCollection = async (doc) => {

        //firestore add collection
        const createdAt = timestamp.fromDate(new Date())
        projectFirestore.collection(collection).doc().set({...doc, createdAt})
        .then(() => {
            console.log("Document successfully written!");
        })
        .catch((error) => {
            console.error("Error writing document: ", error);
        });


    }

    // get document

    const getDocument = async(id) => {
        // firestore get document
        var docRef = projectFirestore.collection(collection).doc(id);

            docRef.get().then((doc) => {
                if (doc.exists) {
                    console.log("Document data:", doc.data());
                    dispatch({type: "GET_DOCUMENT", payload: doc.data()})
                } else {
                    // doc.data() will be undefined in this case
                    console.log("No such document!");
                }
            }).catch((error) => {
                console.log("Error getting document:", error);
                dispatch({type: "ERROR", payload: error})
            });
    }


    // add document
    const addDocument = async (doc) => {
        dispatch({
            type: "IS_PENDING"
        })



        try {
            const createdAt = timestamp.fromDate(new Date())
            const docSnapShot = await ref.add({...doc, createdAt})

            dispatchIfNotCancelled({type: "ADDED_DOCUMENT", payload: docSnapShot})


        } catch(err) {
            dispatchIfNotCancelled({type: "ERROR", payload: err.message})
        }

    }


    // delete document
    const deleteDocument = async (id) => {

        dispatch({type: "IS_PENDING"})


        
        try {
            await ref.doc(id).delete()

            dispatchIfNotCancelled({type: "DELETED_DOCUMENT"})
        } catch(err) {
            dispatchIfNotCancelled({type: "ERROR", payload: err.message})
        }

    }


    useEffect(() => {
        return () => setIsCancelled(true)
    },[])

    return {
        addCollection,
        addDocument,
        deleteDocument,
        response,
        addGenericCollection,
        getDocument
    }
}