import { useState, useEffect } from 'react';
import { projectAuth, projectFirestore } from '../firebase/config';
import { useAuthContext } from './useAuthContext';

export const useSignup = () => {


    const [error, setError] = useState(null)
    const [isPending, setIsPending] = useState(false);
    const [isCancelled, setIsCancelled] = useState(false)

    const { dispatch } = useAuthContext()

    const signup = async (email, password, displayName, role) => {
        setError(null)
        setIsPending(true)

        try {
            // signup user
          const response = await projectAuth.createUserWithEmailAndPassword(email, password)
          console.log(response.user)

          if(!response) {
              throw new Error("impossible de terminer l'inscription")
          }

          // add display name to user
          await response.user.updateProfile({ displayName, role })

          // add users document
            projectFirestore.collection('users').doc(response.user.uid).set({
                name: displayName,
                email,
                role,
                authId: response.user.uid
            })

          

          // dispatch login action
          dispatch({
              type: 'LOGIN',
              payload: response.user
          })

          if (!isCancelled) {
            setIsPending(false)
            setError(null)
        }

        } catch(err) {
            if (!isCancelled) {
                console.error(err)
                setError(err.message)
                setIsPending(false)
            }
        }
    }


    
    useEffect(() => {
        
        return () => setIsCancelled(true)
    },[])

    return { error, isPending, signup}

}