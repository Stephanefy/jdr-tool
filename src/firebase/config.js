import firebase from 'firebase/app';
import 'firebase/firestore'
import 'firebase/auth'

const firebaseConfig = {
    apiKey: "AIzaSyBYwQMdKSIQ_n9aSZyz2selHFQODqYJ73k",
    authDomain: "finance-tracker-a1e18.firebaseapp.com",
    projectId: "finance-tracker-a1e18",
    storageBucket: "finance-tracker-a1e18.appspot.com",
    messagingSenderId: "859982516762",
    appId: "1:859982516762:web:e5097c3773f4275f4177a3"
  };


  // init firebase

  firebase.initializeApp(firebaseConfig)

  // init service

  const projectFirestore = firebase.firestore()
  const projectAuth = firebase.auth()

  // timestamp
  const timestamp = firebase.firestore.Timestamp

  export { projectFirestore, projectAuth, timestamp }