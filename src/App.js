import { BrowserRouter as Router, Switch, Route, Redirect} from 'react-router-dom';
import Login from './pages/login/Login'
import Home from './pages/home/Home'
import Signup from './pages/signup/Signup';
import Navbar from './components/Navbar';
import { useAuthContext } from './hooks/useAuthContext';
import WorldCreation from './pages/master/WorldCreation';
import WorldDetails from './pages/master/WorldDetails';
import SceneCreation from './pages/master/SceneCreation';
import SceneDetails from './pages/master/SceneDetails';

function App() {



  const { authIsReady, user } = useAuthContext()

  return (
    <div className="App">
    {
      authIsReady && (
        <Router>
          <Navbar/>
            <Switch>
              <Route exact path="/s'enregistrer">
                {user && <Redirect to="/"/>}
                {!user && <Signup/>}
              </Route>
              <Route exact path="/connexion">
                {user && <Redirect to="/"/>}
                {!user && <Login/>}
              </Route>
              <Route exact path="/nouvel-univers">
                {user && <WorldCreation/>}
                {!user && <Login/>}
              </Route>
              <Route exact path="/univers/:id">
                {user && <WorldDetails/>}
                {!user && <Login/>}
              </Route>
              <Route exact path="/scene/:id">
                {user && <SceneDetails/>}
                {!user && <Login/>}
              </Route>
              <Route exact path="/univers/:id/nouveau-chapitre">
                {user && <SceneCreation/>}
                {!user && <Login/>}
              </Route>
              <Route path="/">
                {!user && <Redirect to="/s'enregistrer" />}
                {user && <Home />}
              </Route>
            </Switch>
        </Router>

      )
    }

    </div>
  );
}

export default App
