import React, { useState } from 'react'
import styles from './Signup.module.css'
import { useSignup } from '../../hooks/useSignUp'

const Signup = () => {
    const [displayName, setDisplayName] = useState('')
    const [email, setEmail] = useState('')
    const [password, setPassword] = useState('')
    const [role, setRole] = useState('')

    const { signup, isPending, error } = useSignup()

    const handleMasterChange = () => {
        setRole("master")
    }
    const handlePlayerChange = () => {
        setRole("player")
    }
  
  
    const handleSubmit = (e) => {
        e.preventDefault();

        
        signup(email, password, displayName, role)
    }
  
  
    return (
      <form className={styles['signup-form']} onSubmit={handleSubmit}>
          <h2>S'enregistrer</h2>
          <label>
              <span>Nom utilisateur:</span>
              <input 
                  type="text" 
                  onChange={(e) => setDisplayName(e.target.value)}
                  value={displayName}
                  className="flex-1 focus:ring-indigo-500 focus:border-indigo-500 block w-full min-w-0 rounded sm:text-sm border-gray-300"
              />
          </label>
          <label>
              <span>email:</span>
              <input 
                  type="email" 
                  onChange={(e) => setEmail(e.target.value)}
                  value={email}
                  className="flex-1 focus:ring-indigo-500 focus:border-indigo-500 block w-full min-w-0 rounded sm:text-sm border-gray-300"

              />
          </label>
          <label>
              <span>Mot de passe:</span>
              <input 
                  type="password" 
                  onChange={(e) => setPassword(e.target.value)}
                  value={password}
                  className="flex-1 focus:ring-indigo-500 focus:border-indigo-500 block w-full min-w-0 rounded sm:text-sm border-gray-300"

              />
          </label>
          <fieldset className="border-solid border-2 border-gray-300 rounded">
              <legend>Sélectionnez votre role</legend>
              <div>
                <input 
                    type="radio"
                    name="role" 
                    onChange={handleMasterChange}
                    value={role === "master"}
                    className={styles['radio-selector']}
                />
                <label className={styles.checkboxes} htmlFor="master">Maître de jeu</label>

              </div>
              <div>
                <input 
                    type="radio" 
                    name="role"
                    onChange={handlePlayerChange}
                    value={role === "player"}
                    className={styles['radio-selector']}
                />
                <label className={styles.checkboxes} htmlFor="player">Joueur</label>

              </div>
          </fieldset>
          {!isPending && <button className="btn">Connexion</button> }
          { isPending && <button className='btn' disabled>loading</button>}
          { error && <p>{error}</p>}
      </form>
    )
}

export default Signup