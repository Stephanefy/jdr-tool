import React, { useMemo, useState } from 'react'
import { useLocation } from 'react-router-dom'
import SceneModal from '../../components/modals/SceneModal'
import RichTextExample from '../../components/editorjs/RichTextEditor'

const SceneDetail = () => {

  

  const [open, setOpen] = useState(false)

  const location = useLocation()


  const { sceneInfo } = location.state


  console.log('from edition mode',sceneInfo)



  return (
    <>
        <div className='flex justify-between w-6/12 mx-auto mb-2 '>
            <h1 className='text-2xl'>{sceneInfo.title}</h1>
            <button className='css-button-neumorphic' onClick={() => setOpen(true)}>Mode édition</button>
        </div>
        <main className='w-6/12 mx-auto'>
          <RichTextExample sceneInfo={sceneInfo.content} isReadOnly />
        </main>
        {
            open && (
            <SceneModal open={open} setOpen={setOpen} sceneInfo={sceneInfo.content}/>
            )
        }
    </>
  )
}



export default SceneDetail