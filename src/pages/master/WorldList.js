import React from 'react'
import WorldCard from '../../components/MasterDashboard/worldlist/WorldCard'

const WorldList = ( {worlds} ) => {
  return (
    <div class="flex flex-col flex-wrap items-center justify-center w-12/12 lg:flex-row lg:items-start h-screen">
        {worlds?.map(world => (
            <WorldCard world={world} />
        ))}
    </div>
  )
}

export default WorldList