import React, {useContext} from 'react';
import { WorldContext } from '../../context/WorldContext';
import styles from '../home/Home.module.css'

const CharacterPreview = () => {

  const { characterTraits, globalWorldDetail } = useContext(WorldContext)


//   const {characterTraits} = globalWorldDetail


  return (
    <div className={styles['character-preview']}>
        <ul className={styles['character-preview']}>
            <li>
                <h3>Nom du personnage: </h3>
                <p>{characterTraits?.name}</p>
            </li>
                <h3>Caractéristiques du personnage: </h3>
            {characterTraits?.attributes?.length > 0 && characterTraits?.attributes?.map(attr => (
                <li>{attr.name}: {attr.points}</li>
            ))}

        </ul>
    </div>
  )
}

export default CharacterPreview