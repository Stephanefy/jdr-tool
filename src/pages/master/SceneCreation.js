import React, { useEffect, useState, useContext } from 'react'
import styles from '../home/Home.module.css'
import { useLocation } from 'react-router-dom'
import CharacterCard from '../../components/MasterDashboard/worldlist/CharacterCard'
import RichTextExample from '../../components/editorjs/RichTextEditor'
import BaseModal from '../../components/modals/BaseModal'
import { useFirestore } from '../../hooks/useFirestore'
import { WorldDetailContext } from '../../context/WorldDetailContext';
import { useHistory } from 'react-router-dom'

const SceneCreation = () => {


    const { worldDetail } = useContext(WorldDetailContext)

    const [open, setOpen] = useState(false)
    const [url, setUrl] = useState('')
    const [title, setTitle] = useState('')
    const [description, setDescription] = useState('')


    const history = useHistory()


    useEffect(() => {
      console.log(worldDetail)
    }, [])


    const { addGenericCollection } = useFirestore('scenes')
   

    const handleSubmit = (e) =>  {

      e.preventDefault();

      const SceneContent = JSON.parse(localStorage.getItem('content')) || []
      
      if (SceneContent.length > 0) {
          const data = {
            title,
            description,
            content: SceneContent,
            world_id: worldDetail.id
          }

          
          addGenericCollection(data)

          const location = {
            pathname: `/univers/${worldDetail.id}`,
            state: { worldInfo: {...worldDetail} }
          }

          history.push(location)

      }

      localStorage.removeItem('content')
      

    }

    return (
        <div className="flex justify-center flex-col items-center max-content">
            <h1 className="text-2xl mb-6">Créer une Scène</h1>
            <form 
            onSubmit={handleSubmit}
            className="flex flex-col justify-start w-10/12 my-4">
                <label htmlFor="">Nom de la scène</label>
                <input 
                  type="text" 
                  value={title} 
                  className="border-none shadow-lg p-2"
                  onChange={(e) => setTitle(e.target.value)}
                  />
                <label htmlFor="">Description de la scène</label>
                <textarea 
                  value={description} 
                  rows={10} 
                  cols={20} 
                  className="border-none shadow-lg p-2"
                  onChange={(e) => setDescription(e.target.value)}
                />
                <div className="flex justify-end my-2">
                  <button type="submit" className="css-button-neumorphic">Créer</button>
                </div>
              </form>
                <section className="w-10/12">
                    <RichTextExample
                        setOpen={setOpen}
                        url={url}
                        setUrl={setUrl}
                        open={open}
                    />
                </section>
        </div>
    )
}

export default SceneCreation
