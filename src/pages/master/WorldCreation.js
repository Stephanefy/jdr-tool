import React, { useContext } from 'react'
import StepTwo from '../../components/MasterDashboard/StepTwo'
import StepOne from '../../components/MasterDashboard/StepOne'
import StepThree from '../../components/MasterDashboard/StepThree'
import styles from '../home/Home.module.css'
import cx from 'classnames'
import { WorldContext } from '../../context/WorldContext'
import { useFirestore } from '../../hooks/useFirestore'
import { useAuthContext } from '../../hooks/useAuthContext'
import WorldPreview from './WorldPreview'
import CharacterPreview from './CharacterPreview'
import { useHistory } from 'react-router-dom'
import useWindowSize from '../../hooks/useWindowSize'

const WorldCreation = () => {
    const { step, setStep, globalWorldDetail, setGlobalWorldDetail } = useContext(WorldContext)

    const { width } = useWindowSize()


    const history = useHistory()

    const { addCollection } = useFirestore('world')
    const { user } = useAuthContext()

    const handleWorldCreation = () => {
        console.log('globaleWorld detail', globalWorldDetail)
        const { world, characters, numPlayers, story } = globalWorldDetail
        localStorage.removeItem('draft')

        const doc = {
            uid: user.uid,
            world,
            numPlayers,
            characters,
            story,
        }

        addCollection(doc)

        setGlobalWorldDetail({
            world: '',
            numPlayers: 0,
            story: '',
            characters: []
        })

   


        history.push('/')
    }

    const handlePreviousStep = () => {
        step === 1 ? setStep(1) : setStep((prevStep) => prevStep - 1)
    }

    const handleNextStep = () => {
        step >= 3 ? setStep(3) : setStep((prevStep) => prevStep + 1)
    }

    return (
        <div className={styles.container}>
            <div className={styles.content}>
            {
                width <= 1024 && (
                    <div className={styles.sidebar}>
                        <div>
                            {step === 1 && <WorldPreview />}
                            {step === 2 && <CharacterPreview />}
                        </div>
                    </div>
                )
            }
                <div className={styles['block-timelines']}>
                    <div className={styles['timeline-wrapper']}>
                        <button className={styles['timeline-button']} onClick={() => setStep(1)}>
                            <span
                                className={cx(styles['timeline-step'], {
                                    [styles['step-active']]: step === 1,
                                })}
                            >
                                <span>1</span>
                            </span>
                        </button>
                        <button className={styles['timeline-button']} onClick={() => setStep(2)}>
                            <span
                                className={cx(styles['timeline-step'], {
                                    [styles['step-active']]: step === 2,
                                })}
                            >
                                <span>2</span>
                            </span>
                        </button>
                        <button className={styles['timeline-button']} onClick={() => setStep(3)}>
                            <span
                                className={cx(styles['timeline-step'], {
                                    [styles['step-active']]: step === 3,
                                })}
                            >
                                <span>3</span>
                            </span>
                        </button>
                    </div>
                </div>
                <div className={styles['btn-group']}>
                    {step > 1 && (
                        <button
                            className={styles['css-button-neumorphic']}
                            onClick={handlePreviousStep}
                        >
                            Précédent
                        </button>
                    )}
                    {step < 3 && (
                        <button
                            className={cx(
                                styles['css-button-neumorphic'],
                                styles.nextBtn
                            )}
                            onClick={handleNextStep}
                        >
                            Suivant
                        </button>
                    )}
                    {step === 3 && (
                        <button
                            className={styles['css-button-neumorphic-validate']}
                            onClick={handleWorldCreation}
                        >
                            Créer cet univers
                        </button>
                    )}
                </div>
                {step === 1 && <StepOne />}
                {step === 2 && <StepTwo />}
                {step === 3 && <StepThree />}


            </div>
            {
                width > 1024 && (
                    <div className={styles.sidebar}>
                        <div>
                            {step === 1 && <WorldPreview />}
                            {step === 2 && <CharacterPreview />}
                        </div>
                    </div>
                )
            }
        </div>
    )
}

export default WorldCreation
