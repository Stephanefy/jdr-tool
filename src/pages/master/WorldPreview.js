import React, {useContext} from 'react';
import { WorldContext } from '../../context/WorldContext';
import styles from '../home/Home.module.css'

const WorldPreview = () => {

    const { world, numPlayers, story } = useContext(WorldContext)


  return (
    <div>
        <ul className={styles['world-preview']}>
            <li>
                <h3>Nom de l'univers: </h3>
                <p>{world}</p>
            </li>
            <li>
                <h3>Nombre de joueurs: </h3>
                <p>{numPlayers}</p>
            </li>
            <li>
                <h3>Résumé du scénario: </h3>
                <p>{story}</p>
            </li>
        </ul>
    </div>
  )
}

export default WorldPreview