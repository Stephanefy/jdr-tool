import React, { useState, useContext, useEffect } from 'react'
import { useFirestore } from '../../hooks/useFirestore'
import { useAuthContext } from '../../hooks/useAuthContext'
import { useCollection } from '../../hooks/useCollection'
import { useHistory } from 'react-router-dom'
import cx from 'classnames'
import { WorldDetailContextProvider } from '../../context/WorldDetailContext'

import WorldList from './WorldList'


const MasterDashboard = () => {
    const history = useHistory()
    const { user } = useAuthContext()

    const { documents, error } = useCollection('world', ['uid','==', user.uid])

    const { addCollection } = useFirestore('world')

    const handleClick = () => {
        history.push('/nouvel-univers')
    }

    console.log(documents)

    return (
        <div>
            <main>
                <div className="flex flex-col items-center justify-center mb-4">
                    <h1 className="text-2xl text-white">Salut Maître de jeu</h1>
                    <button
                        className='css-button-neumorphic mt-4'
                        onClick={handleClick}
                    >
                        Créer un nouvel univers
                    </button>
                </div>

                <nav></nav>
                    <WorldList worlds={documents} />
            </main>
        </div>
    )
}

export default MasterDashboard
