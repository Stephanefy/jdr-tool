import styles from './Home.module.css'
import MasterDashboard from '../master/MasterDashboard'
import PlayerDashboard from './PlayerDashboard'
import WorldPreview from '../master/WorldPreview'
import { WorldContext } from '../../context/WorldContext'

import React, {useState, useEffect, useContext} from 'react'
import { useAuthContext } from '../../hooks/useAuthContext'
import { useCollection } from '../../hooks/useCollection'
import CharacterPreview from '../master/CharacterPreview'
import background from "../../images/home-bg.jpg";

const Home = () => {

  const  { user } = useAuthContext()

  const { step } = useContext(WorldContext)


  const { documents, error } = useCollection(
    "users",
    ["authId","==", user.uid],
    // ["createdAt", "desc"]
    )
  const [role, setRole] = useState("")
  


  useEffect(() => {
    if (documents) {
        documents[0] && setRole(documents[0].role)
    }
    
  },[documents])

  console.log(documents)

  console.log(role)

  return (
    <div className="master-dashboard" style={{
      backgroundImage: `url(${background})`,
      backgroundSize: 'cover',
      backgroundRepeat:'no-repeat',
      }}>
      <div>
        { error && <p>{error}</p>}
        { role === "master" && <MasterDashboard/>}
        { role === "player" && <PlayerDashboard/>}
      </div>
    </div>
  )
}

export default Home