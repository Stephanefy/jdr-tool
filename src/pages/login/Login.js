import React, { useState } from 'react'
import { useLogin } from '../../hooks/useLogin'
import FormBase from '../../components/forms/FormBase'

import styles from './Login.module.css'

const Login = () => {
    const [email, setEmail] = useState('')
    const [password, setPassword] = useState('')

    const { login, error, isPending } = useLogin()

    const handleSubmit = (e) => {
        e.preventDefault()
        login(email, password)
    }

    return (
        <>
            <form className={styles['login-form']} onSubmit={handleSubmit}>
                <h2>Connexion</h2>
                <label>
                    <span>email:</span>
                    <input
                        type="email"
                        onChange={(e) => setEmail(e.target.value)}
                        value={email}
                        className="flex-1 focus:ring-indigo-500 focus:border-indigo-500 block w-full min-w-0 rounded sm:text-sm border-gray-300"
                    />
                </label>
                <label>
                    <span>Mot de passe:</span>
                    <input
                        type="password"
                        onChange={(e) => setPassword(e.target.value)}
                        value={password}
                        className="flex-1 focus:ring-indigo-500 focus:border-indigo-500 block w-full min-w-0 rounded sm:text-sm border-gray-300"
                    />
                </label>
                {!isPending && <button className="btn">Connexion</button>}
                {isPending && (
                    <button disabled className="btn">
                        Connexion en cours
                    </button>
                )}
                {error && <p>{error}</p>}
            </form>
        </>
    )
}

export default Login
